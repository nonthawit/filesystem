package Run;

import java.util.HashMap;
import java.util.Map;

import Controller.DiskOperations;
import block.Block;
import block.SuperBlock;

public class FileSystem {
	private Block[] blocks;
	private SuperBlock superBlock;
	private int blockCount;
	private int freeBlockBitmapBlockCount;
	private int startCheckFreeBitmap;
	private int rootBlockIndex;
	private Map<String,Integer> openFileTable = new HashMap<String,Integer>();
	
//	private int diskSize = 1073741824; // default 1*1024*1024*1024 byte
	private int diskSize = 64*1024*1024; // 64MB
//	private int diskSize = 100*1024*1024; // 100MB
	private int blockSize = 4096; // block size = 256B, 1KB, 2KB
//	private int blockSize = 2048; // block size = 256B, 1KB, 2KB
//	private int blockSize = 1024; // block size = 256B, 1KB, 2KB
//	private int blockSize = 256; // block size = 256B, 1KB, 2KB
	private DiskOperations dO;

	public FileSystem(int diskSize, int blockSize) {
		this.diskSize = diskSize;
		this.blockSize = blockSize;
		this.dO = new DiskOperations(this);
	}
	
	public Block[] getBlocks() {
		return blocks;
	}
	
	public SuperBlock getSuperBlock() {
		return superBlock;
	}
	
	public Map<String, Integer> getOpenFileTable() {
		return openFileTable;
	}

	public DiskOperations getdO() {
		return dO;
	}
	
	public int getBlockCount() {
		return blockCount;
	}
	
	public int getDiskSize() {
		return diskSize;
	}
	
	public int getBlockSize() {
		return blockSize;
	}
	
	public int getFreeBlockBitmapBlockCount() {
		return freeBlockBitmapBlockCount;
	}
	
	public int getStartCheckFreeBitmap() {
		return startCheckFreeBitmap;
	}
	
	public void setSuperBlock(SuperBlock superBlock) {
		this.superBlock = superBlock;
	}
	
	public void setBlocks(Block[] blocks) {
		this.blocks = blocks;
	}

	public void setBlockCount(int blockCount) {
		this.blockCount = blockCount;
	}
	
	public void setFreeBlockBitmapBlockCount(int freeBlockBitmapBlockCount) {
		this.freeBlockBitmapBlockCount = freeBlockBitmapBlockCount;
	}

	public void setStartCheckFreeBitmap(int startCheckFreeBitmap) {
		this.startCheckFreeBitmap = startCheckFreeBitmap;
	}


	public void setRootBlockIndex(int i) {
		this.rootBlockIndex = i;
	}
	public int getRootBlockIndex(){
		return this.rootBlockIndex;
	}

}
