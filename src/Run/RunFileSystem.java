package Run;

import Controller.Console;
import Controller.DiskOperations;
import Controller.FileOperations;
import Controller.FileSystemOperations;

public class RunFileSystem {
	public static void main(String[] args) {
		Console console = new Console();
		console.init();
		console.readFile("input2.txt");
	}
}
