package Controller;

import java.util.ArrayList;
import java.util.Observable;

import Model.BitmapManager;
import Model.BlockManager;
import Run.FileSystem;
import block.Block;
import block.DirectoryDataBlock;
import block.DirectoryMetadataBlock;
import block.FileDataBlock;
import block.FileMetadataBlock;
import block.FreeBlockBitmap;

public class FileOperations extends Observable{

	public Block[] blocks;
	private FileSystem fs;
	private BlockManager bsm;
	private DiskOperations dO;
	private String n = System.getProperty("line.separator");

	public FileOperations(FileSystem fs) {
		this.fs = fs;
		this.blocks = this.fs.getBlocks();
		this.bsm = new BlockManager(this.fs.getRootBlockIndex(), this.blocks);
		this.dO = fs.getdO();
		
	}

	public void setConsole(Console console) {
		addObserver(console);
	}

	public void createdirectory(String str) {
		ArrayList<Integer> indexes = this.bsm.getBlockIndexesFromPath(str);

		DirectoryDataBlock ddb;
		if (indexes.size() > 1) {
			int parent = indexes.get(indexes.size() - 2);
			// Get parent dmb and ddb
			DirectoryMetadataBlock dmb = (DirectoryMetadataBlock) dO
					.readdisk(parent);
			ddb = (DirectoryDataBlock) dO.readdisk(dmb.getDdbAddress());
			dO.writedisk(dmb.getDdbAddress());



		} else {
			// Get ddb
			ddb = (DirectoryDataBlock) dO.readdisk(this.fs.getRootBlockIndex());
			dO.writedisk(this.fs.getRootBlockIndex());
		}
		// Get the new directory name
		String dirName = this.bsm.getLastElementNameFromPath(str);

		// return the freeblockbitmap index return a block index that is free
		int[] index = this.getAFreeBlockIndex();

		// update ddb
		ddb.addDmbChildAddress(dirName, index[1]);
		ddb.addDirName(dirName);
		// update freeblockbitmap
		FreeBlockBitmap fbb = (FreeBlockBitmap) dO.readdisk(index[0]);
		dO.writedisk(index[0]);
		fbb.setUseBit(index[1]);

		// create DirectoryMetadataBlock and add it to the block
		DirectoryMetadataBlock dmb = new DirectoryMetadataBlock(dirName);
		int freeBlockmapIndex = index[0] - 1;
		int actualAddress = (fs.getBlockSize()*8 * freeBlockmapIndex) + index[1];
		this.blocks[actualAddress] = dmb;
		dO.writedisk(actualAddress);

		// create DirectoryDataBlock and add it to the block add dmb
		index = this.getAFreeBlockIndex();

		fbb = (FreeBlockBitmap) dO.readdisk(index[0]);
		dO.writedisk(index[0]);
		fbb.setUseBit(index[1]);

		ddb = new DirectoryDataBlock();
		freeBlockmapIndex = index[0] - 1;
		actualAddress = (fs.getBlockSize()*8 * freeBlockmapIndex) + index[1];
		this.blocks[actualAddress] = ddb;
		dO.writedisk(actualAddress);

		dmb.setDdbAddress(actualAddress);
	}

	public void createfile(String str) {
		ArrayList<Integer> indexes = this.bsm.getBlockIndexesFromPath(str);

		DirectoryDataBlock ddb;
		if (indexes.size() > 1) {
			int parent = indexes.get(indexes.size() - 2);
			// Get parent dmb and ddb
			DirectoryMetadataBlock dmb = (DirectoryMetadataBlock) dO
					.readdisk(parent);
			ddb = (DirectoryDataBlock) dO.readdisk(dmb.getDdbAddress());
			dO.writedisk(dmb.getDdbAddress());

		} else {
			// Get ddb
			ddb = (DirectoryDataBlock) dO.readdisk(this.fs.getRootBlockIndex());
			dO.writedisk(this.fs.getRootBlockIndex());
		}
		// Get the new file name
		String fileName = this.bsm.getLastElementNameFromPath(str);

		// return the freeblockbitmap index return a block index that is free
		int[] index = this.getAFreeBlockIndex();

		// update ddb
		ddb.addFmbChildAddress(fileName, index[1]);
		ddb.addFileName(fileName);

		// update freeblockbitmap
		FreeBlockBitmap fbb = (FreeBlockBitmap) dO.readdisk(index[0]);
		dO.writedisk(index[0]);
		fbb.setUseBit(index[1]);

		// create FileMetadataBlock and add it to the block
		FileMetadataBlock fmb = new FileMetadataBlock(fileName);
		int freeBlockmapIndex = index[0]-1;
		this.blocks[(fs.getBlockSize()*8 * freeBlockmapIndex) + index[1]] = fmb;

		// Add file to OpenFileTable
		fs.getOpenFileTable().put(fileName, 0);
	}

	public void deletefile(String str) {
		ArrayList<Integer> indexes = this.bsm.getBlockIndexesFromPath(str);
		DirectoryDataBlock ddb;
		if (indexes.size() > 1) {
			int parent = indexes.get(indexes.size() - 2);

			// Get parent dmb and ddb
			DirectoryMetadataBlock dmb = (DirectoryMetadataBlock) dO
					.readdisk(parent);
			ddb = (DirectoryDataBlock) dO.readdisk(dmb.getDdbAddress());
			dO.writedisk(dmb.getDdbAddress());
		} else {
			// Get root ddb
			ddb = (DirectoryDataBlock) dO.readdisk(this.fs.getRootBlockIndex());
			dO.writedisk(this.fs.getRootBlockIndex());
		}
		// get fmb
		String filename = this.bsm.getLastElementNameFromPath(str);
		int fmbIndex = ddb.getFmbChildAddressByName(filename);
		FileMetadataBlock fmb = (FileMetadataBlock) fs.getdO().readdisk(fmbIndex);

		// delete file
		ddb.deleteFile(filename);
		if (fmb.getLinkCount() != 0) {
			fmb.decrementLinkCount();
			dO.writedisk(fmbIndex);
		} else {
			ArrayList<Integer> deleteIndexes = fmb.getFdbAddress();
			deleteIndexes.add(fmbIndex);
			Block[] blocks = fs.getBlocks();
			BitmapManager bm = new BitmapManager(blocks, 1, fs.getBlockCount()+1);
			for (Integer i : deleteIndexes) {
				blocks[i] = null;
				//set freebit
				int[] freebit = bm.getFreeBlockBitmapAndBitIndex(i);
				((FreeBlockBitmap)dO.readdisk(freebit[0])).setFreeBit(freebit[1]);
				dO.writedisk(freebit[0]);
			}

		}

	}

	public ArrayList<Integer> openfile(String str) {
		// Add file to OpenFileTable
		String filename = this.bsm.getLastElementNameFromPath(str);
		ArrayList<Integer> indexes = fileExist(str);
		if (indexes != null) {
			fs.getOpenFileTable().put(filename, 0);
			return indexes;
		}
		return null;

	}

	private ArrayList<Integer> fileExist(String str) {
		ArrayList<Integer> indexes = this.bsm.getBlockIndexesFromPath(str);
		int count = str.split("\\\\").length;
		if (indexes.get(count - 2) != null) {
			return indexes;
		}
		return null;
	}

	public void writefile(String str, Integer size) {
		String filename = this.bsm.getLastElementNameFromPath(str);
		if (fs.getOpenFileTable().get(filename) != null) {
			// openfile ==> use method "getBlockIndexesFromPath" in BlockManager
			ArrayList<Integer> indexes = openfile(str);

			DirectoryDataBlock ddb;
			if (indexes.size() > 1) {
				int parent = indexes.get(indexes.size() - 2);

				// Get parent dmb and ddb
				DirectoryMetadataBlock dmb = (DirectoryMetadataBlock) dO
						.readdisk(parent);
				ddb = (DirectoryDataBlock) dO.readdisk(dmb.getDdbAddress());
				// dO.writedisk(dmb.getDdbAddress());

			} else {
				// Get root ddb
				ddb = (DirectoryDataBlock) dO.readdisk(this.fs
						.getRootBlockIndex());
				// dO.writedisk(this.fs.getRootBlockIndex());
			}
			// get fmb
			FileMetadataBlock fmb = (FileMetadataBlock) fs.getdO().readdisk(
					ddb.getFmbChildAddressByName(filename));

			int fdbBlockCount = (int) Math.ceil((double)size / fs.getBlockSize());

			int total = 0;
			for (int i = 0; i < fdbBlockCount; i++) {
				int[] index = this.getAFreeBlockIndex();

				// find acctual address
				int freeBlockmapIndex = index[0] - 1;
				int actualIndex = (freeBlockmapIndex * fs.getBlockSize()*8)
						+ index[1];
				if ((size - total) < fs.getBlockSize()) {
					// last fdbBlock (not add full size)
					fs.getBlocks()[actualIndex] = new FileDataBlock(size
							- total);
				} else {
					fs.getBlocks()[actualIndex] = new FileDataBlock(
							fs.getBlockSize());
				}

				dO.writedisk(actualIndex);
				fmb.addFdbAddress(actualIndex);

				// update freeblockbitmap
				FreeBlockBitmap fbb = (FreeBlockBitmap) dO.readdisk(index[0]);
				dO.writedisk(index[0]);
				fbb.setUseBit(index[1]);

				if ((size - total) < fs.getBlockSize()) {
					break;
				}

				total += fs.getBlockSize();
			}
			// update file size
			fmb.addFileSize(size);

		}

	}

	public void closefile(String str) {
		String filename = this.bsm.getLastElementNameFromPath(str);
		fs.getOpenFileTable().remove(filename);
	}

	public void readfile(String str, Integer size) {
		String filename = this.bsm.getLastElementNameFromPath(str);
		if (fs.getOpenFileTable().get(filename) != null) {
			// openfile ==> use method "getBlockIndexesFromPath" in BlockManager
			ArrayList<Integer> indexes = openfile(str);
			if (indexes != null) {
				DirectoryDataBlock ddb;
				if (indexes.size() > 1) {
					int parent = indexes.get(indexes.size() - 2);
					DirectoryMetadataBlock dmb = (DirectoryMetadataBlock) dO
							.readdisk(parent);
					ddb = (DirectoryDataBlock) dO.readdisk(dmb.getDdbAddress());
				} else {
					ddb = (DirectoryDataBlock) dO.readdisk(fs
							.getRootBlockIndex());
				}
				// get fmb
				FileMetadataBlock fmb = (FileMetadataBlock) fs.getdO()
						.readdisk(ddb.getFmbChildAddressByName(filename));
				if (size < fmb.getFileSize()) {
					fs.getOpenFileTable().put(filename, size);
				}
			}
		}

	}

	public void seekfile(String str, Integer position) {
		String filename = this.bsm.getLastElementNameFromPath(str);
		int oldPostion = fs.getOpenFileTable().get(filename);
		fs.getOpenFileTable().put(filename, position);
		setChanged();
		notifyObservers("previous postion : \t" + oldPostion + n + "current position : \t" + position + n);
	}

	public void linkfile(String oldStr, String newStr) {
		ArrayList<Integer> oldIndexes = this.bsm
				.getBlockIndexesFromPath(oldStr);
		ArrayList<Integer> newIndexes = this.bsm
				.getBlockIndexesFromPath(newStr);
		DirectoryDataBlock oldDdb;
		DirectoryDataBlock newDdb;
		if (oldIndexes.size() > 1) {
			int parent = oldIndexes.get(oldIndexes.size() - 2);

			// Get parent dmb and ddb
			DirectoryMetadataBlock oldDmb = (DirectoryMetadataBlock) dO
					.readdisk(parent);
			oldDdb = (DirectoryDataBlock) dO.readdisk(oldDmb.getDdbAddress());
			// dO.writedisk(oldDmb.getDdbAddress());

		} else {
			// Get ddb
			oldDdb = (DirectoryDataBlock) dO.readdisk(this.fs
					.getRootBlockIndex());
			// dO.writedisk(this.fs.getRootBlockIndex());
		}
		if (newIndexes.size() > 1) {
			int parent = newIndexes.get(newIndexes.size() - 2);
			// Get parent dmb and ddb
			DirectoryMetadataBlock newDmb = (DirectoryMetadataBlock) dO
					.readdisk(parent);
			newDdb = (DirectoryDataBlock) dO.readdisk(newDmb.getDdbAddress());
			dO.writedisk(newDmb.getDdbAddress());

		} else {
			// Get ddb
			newDdb = (DirectoryDataBlock) dO.readdisk(this.fs
					.getRootBlockIndex());
			dO.writedisk(this.fs.getRootBlockIndex());
		}
		// insert a filename and address to old fmb
		String newFileName = this.bsm.getLastElementNameFromPath(newStr);
		String oldFileName = this.bsm.getLastElementNameFromPath(oldStr);
		newDdb.addFileName(newFileName);
		int oldFmbIndex = oldDdb.getFmbChildAddressByName(oldFileName);
		newDdb.addFmbChildAddress(newFileName, oldFmbIndex);
		FileMetadataBlock oldFmb = (FileMetadataBlock) dO.readdisk(oldFmbIndex);
		oldFmb.incrementLinkCount();
		dO.writedisk(oldFmbIndex);
	}

	public int getsize(String str) {
		ArrayList<Integer> indexes = this.bsm.getBlockIndexesFromPath(str);
		int index = indexes.get(indexes.size()-1);
		int fileSize = ((FileMetadataBlock)dO.readdisk(index)).getFileSize();
		setChanged();
		notifyObservers("fileSize : \t" + fileSize + n);
		return fileSize;
	}

	public int getoffset(String str) {
		ArrayList<Integer> indexes = this.bsm.getBlockIndexesFromPath(str);
		int index = indexes.get(indexes.size()-1);
		setChanged();
		notifyObservers("offset : \t" + index + n);
		return index;
	}

	public void listopenfile() {
		String str = "Open File List : \t[";
		for (String name : fs.getOpenFileTable().keySet()) {
			str += name +",";
		}
		str = str.substring(0, str.length()-1);
		str += "]"+n;
		setChanged();
		notifyObservers(str);
	}

	private int[] getAFreeBlockIndex() {
		// find empty block
		int[] empIndex = { 0, 0 };
		outerloop: for (int i = 1; i < fs.getFreeBlockBitmapBlockCount() + 1; i++) {
			FreeBlockBitmap freeBlock = (FreeBlockBitmap) fs.getBlocks()[i];
			empIndex[0] = i;
			for (int j = 0; j < fs.getBlockSize() * 8; j++) {
				if (freeBlock.getFreeBitMap()[j] == true) {
					empIndex[1] = j;
					break outerloop;
				}
			}
		}
		// System.out.println( fs.getFreeBlockBitmapBlockCount());
//		 System.out.println(empIndex[0]+","+empIndex[1]);
//		 System.out.println(fs.getBlocks()[33]);
		return empIndex;
	}
}
