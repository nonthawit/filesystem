package Controller;

import java.util.ArrayList;

import block.Block;
import block.DirectoryDataBlock;
import block.FileMetadataBlock;
import block.FreeBlockBitmap;
import block.SuperBlock;
import Run.FileSystem;

public class FileSystemOperations {

	private String n = System.getProperty("line.separator");

	public void initFileSystem(FileSystem fs) {
		this.initSuperBlock(fs);
		this.initBlocks(fs);
		this.initFreeBlockBitmap(fs);
		this.initRootBlock(fs);
	}

	private void initSuperBlock(FileSystem fs) {
		fs.setSuperBlock(new SuperBlock());
		SuperBlock superBlock = fs.getSuperBlock();
		superBlock.setDiskSize(fs.getDiskSize());
		superBlock.setBlockSize(fs.getBlockSize());
	}

	private void initBlocks(FileSystem fs) {
		SuperBlock superBlock = fs.getSuperBlock();
		// calculate blockCount
		fs.setBlockCount(superBlock.getDiskSize() / superBlock.getBlockSize());
		int blockCount = fs.getBlockCount();
		// create Blocks array
		fs.setBlocks(new Block[blockCount]);
		fs.getBlocks()[0] = superBlock;
	}

	private void initFreeBlockBitmap(FileSystem fs) {
		SuperBlock superBlock = fs.getSuperBlock();
		Block[] blocks = fs.getBlocks();
		// init free block
		int freeBlockBitmapCount = (int)Math.ceil((double)fs.getBlockCount()
				/ (superBlock.getBlockSize() * 8));



		int i;
		for (i = 1; i < freeBlockBitmapCount + 1; i++) {
			blocks[i] = new FreeBlockBitmap(superBlock.getBlockSize() * 8);
		}
		int lastFreeBlock = fs.getBlockCount()
				% (superBlock.getBlockSize() * 8);
//		if (lastFreeBlock != 0 && (fs.getBlockCount() > (superBlock.getBlockSize() * 8))) {
//			blocks[i] = new FreeBlockBitmap(lastFreeBlock);
//			i++;
//		}
//		 System.out.println(fs.getBlockCount());
//		 System.out.println(superBlock.getBlockSize() * 8);
//		 System.out.println("lastFreeBlock:"+lastFreeBlock);

		// Assign used block
		int freeBlockCount = 1;
		int count = 0;
		int limit = 0;
		while (count <= freeBlockBitmapCount ) {
			if (limit > (superBlock.getBlockSize() * 8)-1) {
				freeBlockCount++;
				limit = 0;
			}
			((FreeBlockBitmap) blocks[freeBlockCount]).setUseBit(limit);
			limit++;
			count++;
		}
		// set use for root block
		((FreeBlockBitmap) blocks[freeBlockCount]).setUseBit(limit);
		//System.out.println(limit);
		fs.setFreeBlockBitmapBlockCount(freeBlockBitmapCount);
		fs.setRootBlockIndex(i);
	}

	private void initRootBlock(FileSystem fs) {
		// root directory data
		Block[] blocks = fs.getBlocks();
		blocks[fs.getRootBlockIndex()] = new DirectoryDataBlock();
	}

	public String consistencyCheck(FileSystem fs) {
		String report = "";
		report += "--< Consistency Report >--" + n;
		report += "<<block size =" + fs.getBlockSize() + ">>" + n ;
		report += "Directory :";

		ArrayList<Block> occupiedBlocks = fs.getdO().getAllBlocks();
		ArrayList<DirectoryDataBlock> ddbs = fs.getdO().getDdbBlocks(
				occupiedBlocks);

		Block[] blocks = fs.getBlocks();
		for (DirectoryDataBlock ddb : ddbs) {
			for (String filename : ddb.getFileNames()) {
				int i = ddb.getFmbChildAddressByName(filename);
				report += "[Name: " + '"' + filename + '"' + ", Count: "
						+ ((FileMetadataBlock) blocks[i]).getLinkCount()
						+ ", Start: " + ddb.getFmbChildAddressByName(filename)
						+ ", Size: "
						+ ((FileMetadataBlock) blocks[i]).getFileSize() + "]"
						+ n;
			}
		}
		for (DirectoryDataBlock ddb : ddbs) {
			for (String filename : ddb.getFileNames()) {
				int i = ddb.getFmbChildAddressByName(filename);
				report += "File name\t\t: " + '"' + filename + '"' + n;
				report += "Block occupied: \t(";
				String tmpString = "";
				for (int j : ((FileMetadataBlock) blocks[i]).getFdbAddress()) {
					tmpString += j + ",";
				}
				if(! tmpString.equals("")){
					report += tmpString.substring(0, tmpString.length() - 1);
				}
				report += ")" + n;
			}
		}
		report += "--< End of report"+n;

		return report;
	}

	public double efficiencycheck(FileSystem fs) {
		ArrayList<Block> blocks = fs.getdO().getAllBlocks();
		ArrayList<FileMetadataBlock> fmds = fs.getdO().getFmbBlocks(blocks);
		double totalFileSize = fs.getdO().getTotalFileSize(fmds);
		double eff = totalFileSize / (blocks.size() * fs.getBlockSize());
		return eff;
	}

	public String efficiencyReport(FileSystem fs) {
		String report = "--< Efficiency Report\n";
		ArrayList<Block> blocks = fs.getdO().getAllBlocks();
		report += "Total number of block(s) : " + blocks.size() + n;
		report += "Total number of file(s) : "
				+ fs.getdO().getNumberOfFiles(blocks) + n;
		ArrayList<FileMetadataBlock> fmds = fs.getdO().getFmbBlocks(blocks);
		report += "Total file sizes : " + fs.getdO().getTotalFileSize(fmds) + n;
		double eff = Math.round(efficiencycheck(fs) * 10000.0) / 10000.0;
		report += "Disk space efficiency : " + eff + " %" + n;
		report += "--< End of report" + n;
		return report;
	}

	public String operationReport(String tmpString, FileSystem fs,
			String command) {
		String result = "";
		result += "<<block size =" + fs.getBlockSize() + ">>" + n;
		result += "command entered:\t" + command + n;
		if (tmpString != null) {
			result += tmpString + n;
		}
		result += fs.getdO().getSteps();
		result += "Total operation time (r/w) : "
				+ fs.getdO().getTotalOperation() + n;
		fs.getdO().resetOperation();
		return result;
	}
}
