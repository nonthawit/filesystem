package Controller;

import java.util.ArrayList;

import block.Block;
import block.DirectoryDataBlock;
import block.DirectoryMetadataBlock;
import block.FileDataBlock;
import block.FileMetadataBlock;
import block.FreeBlockBitmap;
import block.SuperBlock;
import Run.FileSystem;

public class DiskOperations {
	ArrayList<String> steps = new ArrayList<String>();
	private FileSystem fs;
	private int totalOperation;
	private String n = System.getProperty("line.separator");

	public DiskOperations(FileSystem fs) {
		this.fs = fs;
	}

	public Block readdisk(int blockNumber) {
		Block block = this.fs.getBlocks()[blockNumber];
		if (block instanceof DirectoryDataBlock) {
			this.steps.add("readdisk(" + blockNumber
					+ ") \t-> directory data block");
		} else if (block instanceof DirectoryMetadataBlock) {
			this.steps.add("readdisk(" + blockNumber
					+ ") \t-> directory metadata block");
		} else if (block instanceof FileDataBlock) {
			this.steps
					.add("readdisk(" + blockNumber + ") \t-> file data block");
		} else if (block instanceof FileMetadataBlock) {
			this.steps.add("readdisk(" + blockNumber
					+ ") \t-> file metadata block");
		} else if (block instanceof SuperBlock) {
			this.steps.add("readdisk(" + blockNumber + ") \t-> super block");
		} else if (block instanceof FreeBlockBitmap) {
			this.steps.add("readdisk(" + blockNumber
					+ ") \t-> free block bitmap");
		}
		totalOperation++;
		return block;

	}

	public void writedisk(int blockNumber) {
		Block block = this.fs.getBlocks()[blockNumber];
		if (block instanceof DirectoryDataBlock) {
			this.steps.add("writedisk(" + blockNumber
					+ ") \t-> directory data block");
		} else if (block instanceof DirectoryMetadataBlock) {
			this.steps.add("writedisk(" + blockNumber
					+ ") \t-> directory metadata block");
		} else if (block instanceof FileDataBlock) {
			this.steps.add("writedisk(" + blockNumber
					+ ") \t-> file data block");
		} else if (block instanceof FileMetadataBlock) {
			this.steps.add("writedisk(" + blockNumber
					+ ") \t-> file metadata block");
		} else if (block instanceof SuperBlock) {
			this.steps.add("writedisk(" + blockNumber + ") \t-> super block");
		} else if (block instanceof FreeBlockBitmap) {
			this.steps.add("writedisk(" + blockNumber
					+ ") \t-> free block bitmap");
		}
		totalOperation++;
	}

	public ArrayList<Block> getAllBlocks() {
		ArrayList<Block> arBlocks = new ArrayList<Block>();
		Block[] blocks = fs.getBlocks();
		for (int i = 0; i < fs.getBlockCount(); i++) {
			if (blocks[i] != null) {
				arBlocks.add(blocks[i]);
			}
		}
		return arBlocks;
	}

	public int getTotalOperation() {
		return totalOperation;
	}

	public void resetOperation() {
		totalOperation = 0;
		steps = new ArrayList<String>();
	}

	public ArrayList<FileMetadataBlock> getFmbBlocks(ArrayList<Block> blocks) {
		ArrayList<FileMetadataBlock> arrFmd = new ArrayList<FileMetadataBlock>();
		for (Block block : blocks) {
			if (block instanceof FileMetadataBlock) {
				arrFmd.add((FileMetadataBlock) block);
			}
		}
		return arrFmd;
	}
	
	public ArrayList<FileDataBlock> getFdbBlocks(ArrayList<Block> blocks) {
		ArrayList<FileDataBlock> arrFdb = new ArrayList<FileDataBlock>();
		for (Block block : blocks) {
			if (block instanceof FileDataBlock) {
				arrFdb.add((FileDataBlock) block);
			}
		}
		return arrFdb;
	}
	
	public ArrayList<DirectoryDataBlock> getDdbBlocks(ArrayList<Block> blocks) {
		ArrayList<DirectoryDataBlock> arrDdb = new ArrayList<DirectoryDataBlock>();
		for (Block block : blocks) {
			if (block instanceof DirectoryDataBlock) {
				arrDdb.add((DirectoryDataBlock) block);
			}
		}
		return arrDdb;
	}

	public int getNumberOfFiles(ArrayList<Block> blocks) {
		int totalFiles = 0;
		for (Block block : blocks) {
			if (block instanceof DirectoryDataBlock) {
				DirectoryDataBlock ddb = (DirectoryDataBlock) block;
				totalFiles += ddb.getNumberOfFiles();
			}
		}
		return totalFiles;
	}

	public int getTotalFileSize(ArrayList<FileMetadataBlock> blocks) {
		int totalSize = 0;
		for (FileMetadataBlock block : blocks) {
			totalSize += block.getFileSize();
		}

		return totalSize;
	}

	public String getSteps() {
		String result ="";
		for(String step:this.steps){
			result += "\t\t\t"+step+n;
		}
		return result;
	}

}
