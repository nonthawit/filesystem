package Controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;

import Run.FileSystem;

public class Console implements Observer{
	private DiskOperations diskOperations;
	private FileOperations fileOperations;
	private FileSystemOperations fileSystemOperations;
	private FileSystem fs;
	private java.lang.reflect.Method method;
	private FileWriter writer;
	private String outputString = "";
	private String n = System.getProperty("line.separator");
	private String tmpString;
	
	public Console() {

	}

	public String getInputString(){
		String filename = "input3.txt";
		String line = null;
		String result = "";
		try {
			InputStream file = getClass().getResourceAsStream(filename);
			BufferedReader br = new BufferedReader(new InputStreamReader(file));

			while ((line = br.readLine()) != null) {
				//callMethod(line.split(" "));
				result += line + n;
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	public void readFile(String filename) {

		String line = null;
		try {
			InputStream file = getClass().getResourceAsStream(filename);
			BufferedReader br = new BufferedReader(new InputStreamReader(file));

			while ((line = br.readLine()) != null) {
				callMethod(line.split(" "));
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(this.outputString);
		this.writeOuput("output");
		this.outputString = "";
	}

	public String run1k(String code){
		fs = new FileSystem(100*1024*1024,1024);
		fileSystemOperations = new FileSystemOperations();
		fileSystemOperations.initFileSystem(fs);
		fileOperations = new FileOperations(fs);
		fileOperations.setConsole(this);
		String[] lines =code.split(n);
		for (String line:lines){
			callMethod(line.split(" "));
		}
		this.writeOuput("1KB");
		String result = this.outputString;
		this.outputString = "";
		return result;
	}
	public String run2k(String code){
		fs = new FileSystem(100*1024*1024,2048);
		fileSystemOperations = new FileSystemOperations();
		fileSystemOperations.initFileSystem(fs);
		fileOperations = new FileOperations(fs);
		fileOperations.setConsole(this);
		String[] lines =code.split(n);
		for (String line:lines){
			callMethod(line.split(" "));
		}
		this.writeOuput("2KB");
		String result = this.outputString;
		this.outputString = "";
		return result;
	}
	public String run4k(String code){
		fs = new FileSystem(100*1024*1024,4096);
		fileSystemOperations = new FileSystemOperations();
		fileSystemOperations.initFileSystem(fs);
		fileOperations = new FileOperations(fs);
		fileOperations.setConsole(this);
		String[] lines =code.split(n);
		for (String line:lines){
			callMethod(line.split(" "));
		}
		this.writeOuput("4KB");
		String result = this.outputString;
		this.outputString = "";
		return result;
	}
	
	
	public void runConsole() {
		Scanner sc = new Scanner(System.in);
		while (true) {
			callMethod(sc.nextLine().split(" "));
		}
	}

	public void callMethod(String[] inputs) {

		try {
			if (inputs[0].equals("0")) {
				this.outputString += fileSystemOperations.consistencyCheck(fs) +n;
				return;
			} else if (inputs.length == 3) {
				if (isInteger(inputs[2])) {
					method = fileOperations.getClass().getMethod(inputs[0],
							String.class, Integer.class);
					method.invoke(fileOperations, inputs[1],
							Integer.valueOf(inputs[2]));
				} else {
					method = fileOperations.getClass().getMethod(inputs[0],
							String.class, String.class);
					method.invoke(fileOperations, inputs[1], inputs[2]);
				}
				this.outputString += fileSystemOperations.operationReport(this.tmpString,fs,inputs[0]+"(\""+inputs[1].substring(1, inputs[1].length())+"\", \""+inputs[2]+"\")")+n;
			} else if (inputs.length == 2) {
				method = fileOperations.getClass().getMethod(inputs[0],
						String.class);
				method.invoke(fileOperations, inputs[1]);
				this.outputString += fileSystemOperations.operationReport(this.tmpString,fs,inputs[0]+"(\""+inputs[1].substring(1, inputs[1].length())+"\")")+n;
			} else {
				if(inputs[0].length() >6){
					method = fileOperations.getClass().getMethod(inputs[0]);
					method.invoke(fileOperations);
					this.outputString += fileSystemOperations.operationReport(this.tmpString,fs,inputs[0]+"()") +n;
				}
			}
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
//			for (int i = 0; i < inputs.length; i++) {
//				System.out.print(inputs[i]);
//			}
			//e.printStackTrace();
			this.tmpString = "There was an error, when trying to "+inputs[0]+".";
			this.outputString += fileSystemOperations.operationReport(this.tmpString,fs,inputs[0]) +n;
			this.tmpString = null;
//			StackTraceElement[] elements = e.getCause().getStackTrace();
//			for(int i=0;i<elements.length;i++){
//				if(elements[i].getClassName().equals("Controller.FileOperations")){
//					System.out.print(elements[i].getMethodName()+n);	
//				}
//			}
			
		} 
		
		ArrayList<String> effList = new ArrayList<String>();
		effList.add("createdirectory");
		effList.add("createfile");
		effList.add("deletefile");
		effList.add("writefile");
		effList.add("linkfile");
		
		this.tmpString = null;
		if(effList.contains(inputs[0]))
			this.outputString += fileSystemOperations.efficiencyReport(fs) + n;

	}

	private void writeOuput(String filename){
		String output = filename+".txt";
		//File file = new File(output);
		
		try {
			Writer out = new BufferedWriter(new OutputStreamWriter(
				    new FileOutputStream(output), "UTF-8"));
			//writer = new FileWriter(file, false);  //True = Append to file, false = Overwrite
			out.write("สมาชิกในกลุ่ม"+n+"1.5510450398 นาย พศิน สุขใจมิตร"+n+"2.5510406666 แม็ก ยาขอบเซิน"+n+"3.5510405660 นนทวิชช์ ดวงสอดศรี"+n+n);
			out.write(this.outputString);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return false;
		} catch (NullPointerException e) {
			return false;
		}
		// only got here if we didn't return false
		return true;
	}

	@Override
	public void update(Observable o, Object arg) {
		this.tmpString  = (String) arg;

	}

	public void init() {
		fs = new FileSystem(100*1024*1024,1024);
		fileSystemOperations = new FileSystemOperations();
		fileSystemOperations.initFileSystem(fs);
		fileOperations = new FileOperations(fs);
		fileOperations.setConsole(this);
	}
}
