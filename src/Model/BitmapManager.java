package Model;

import block.Block;
import block.FreeBlockBitmap;

public class BitmapManager {
	private Block[] blocks;
	private int bitSize;

	public BitmapManager(Block[] blocks,int startIndex,int endIndex){
		this.blocks = blocks;
		this.bitSize = ((FreeBlockBitmap) this.blocks[startIndex]).getFreeBitSize();
	}
	public int[] getFreeBlockBitmapAndBitIndex(int blockIndex){
		int[] result = new int[2];
		int freeBlockBitmapIndex = blockIndex/this.bitSize+1;
		result[0] = freeBlockBitmapIndex;
		int bitIndex = blockIndex%this.bitSize;
		result[1] = bitIndex;
		return result;
	}
}
