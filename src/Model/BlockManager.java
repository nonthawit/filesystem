package Model;

import java.util.ArrayList;

import block.Block;
import block.DirectoryDataBlock;
import block.DirectoryMetadataBlock;
import block.FileMetadataBlock;

public class BlockManager {
	private int rootDdbAddress;
	private Block[] blocks;

	public BlockManager(int rootDdbAddress, Block[] blocks) {
		this.rootDdbAddress = rootDdbAddress;
		this.blocks = blocks;
	}

	public ArrayList<Integer> getBlockIndexesFromPath(String path) {
		// System.out.println(path);
		String[] tokens = path.split("\\\\");

		// System.out.println(tokens.length);
		ArrayList<Integer> indexes = new ArrayList<Integer>();
		DirectoryDataBlock tempDdb = (DirectoryDataBlock) this.blocks[this.rootDdbAddress];
		for (int i = 1; i <= tokens.length - 1; i++) {
			// A file
			// System.out.println(i);
			if (tokens[i].contains(".")) {
				if (tempDdb.hasFileName(tokens[i])) {
					indexes.add(tempDdb.getFmbChildAddressByName(tokens[i]));
				} else {
					indexes.add(null);
				}
			}
			// A directory
			else {
				if (tempDdb.hasDirName(tokens[i])) {
					indexes.add(tempDdb.getDmbChildAddressByName(tokens[i]));
					DirectoryMetadataBlock dmb = (DirectoryMetadataBlock) this.blocks[ tempDdb.getDmbChildAddressByName(tokens[i])];
					tempDdb = (DirectoryDataBlock) this.blocks[dmb.getDdbAddress()];
				} else {
					indexes.add(null);
				}
			}
		}
		return indexes;
	}

	public String getLastElementNameFromPath(String path) {
		String[] tokens = path.split("\\\\");
		return tokens[tokens.length - 1];
	}
}
