package Test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import block.Block;
import block.DirectoryDataBlock;
import block.FileMetadataBlock;
import block.FreeBlockBitmap;
import Controller.DiskOperations;
import Controller.FileOperations;
import Controller.FileSystemOperations;
import Run.FileSystem;

public class FileOperationsTest {

	private FileSystem fs;
	private FileOperations fileOperations;
	@Before
	public void setUp() throws Exception {
		
		this.fs = new FileSystem(1024*1024*1024,1024);
		FileSystemOperations fileSystemOperations = new FileSystemOperations();
		fileSystemOperations.initFileSystem(fs);
		this.fileOperations = new FileOperations(fs);
		
		
	}

//	@Test
//	public void testCreateAFileInRootDir() {
//		this.fileOperations.createfile("\\test2.txt");
//		String expected = "sb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,ddb,fmb,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,";
//		System.out.println(this.getBlock());
//		assertEquals(this.getBlock(), expected, this.getBlock());
//	}
//	@Test
//	public void testCreateADirectoryInRootDir() {
//		this.fileOperations.createdirectory("\\test");
//		String expected = "sb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,ddb,dmb,ddb,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,";
//		//System.out.println(this.getBlock());
//		assertEquals(this.getBlock(), expected, this.getBlock());
//	}
//	@Test
//	public void testCreateAFileInFirstLevelDir() {
//		this.fileOperations.createdirectory("\\test");
//		this.fileOperations.createfile("\\test2.txt");
//		String expected = "sb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,ddb,dmb,ddb,fmb,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,";
//		//System.out.println(this.getBlock());
//		assertEquals(this.getBlock(), expected, this.getBlock());
//	}
//	@Test
//	public void testCreateADirInFirstLevelDir() {
//		this.fileOperations.createdirectory("\\test");
//		this.fileOperations.createdirectory("\\test\\test");
//		String expected = "sb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,ddb,dmb,ddb,dmb,ddb,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,";
//		//System.out.println(this.getBlock());
//		assertEquals(this.getBlock(), expected, this.getBlock());
//	}
//	@Test
//	public void testWriteAFile() {
//		this.fileOperations.createfile("\\test.txt");
//		this.fileOperations.writefile("\\test.txt", 12000);
//		String expected = "sb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,ddb,fmb,fdb,fdb,fdb,fdb,fdb,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,";
//		//System.out.println(this.getBlock());
//		assertEquals(this.getBlock(), expected, this.getBlock());
//	}
//	@Test
//	public void testLinkFileInRootDir() {
//		this.fileOperations.createfile("\\test.txt");
//		this.fileOperations.linkfile("\\test.txt", "\\aCopy.txt");
//		String expected = "sb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,fbb,ddb,fmb,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,";
//		DirectoryDataBlock ddb = (DirectoryDataBlock) this.fs.getBlocks()[33];
//		FileMetadataBlock fmb = (FileMetadataBlock) this.fs.getBlocks()[34];
//		assertEquals(34, ddb.getFmbChildAddressByName("aCopy.txt"));
//		assertEquals(1, fmb.getLinkCount());
//		assertTrue(ddb.hasFileName("aCopy.txt"));
////		System.out.println(this.fs.getBlocks()[34]);
////		System.out.println(fmb.getLinkCount());
////		System.out.println(this.getBlock());
//		assertEquals(this.getBlock(), expected, this.getBlock());
//	}
	private String getBlock(){
		String result = " ";
		int i = 0;
		for (Block block : this.fs.getBlocks()) {
			result += block + ",";
			i++;
			if (i == 20) {
				break;
			}
		}
		
		result += "\n";
		for (int j = 0; j < ((FreeBlockBitmap)this.fs.getBlocks()[1]).getFreeBitMap().length; j++) {
			Integer tmp = ((FreeBlockBitmap)this.fs.getBlocks()[1]).getFreeBitMap()[j]? 1:0;
			result += " "+tmp+" ,";
			if(j == 50){
				break;
			}
		}
//		for (int j = 0; j < ((FreeBlockBitmap)this.fs.getBlocks()[2]).getFreeBitMap().length; j++) {
//			Integer tmp = ((FreeBlockBitmap)this.fs.getBlocks()[2]).getFreeBitMap()[j]? 1:0;
//			result += " "+tmp+" ,";
//		}
		return result;
	}

}
