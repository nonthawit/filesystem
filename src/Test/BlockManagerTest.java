package Test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import Controller.FileOperations;
import Controller.FileSystemOperations;
import Model.BlockManager;
import Run.FileSystem;

public class BlockManagerTest {

	private FileSystem fs;
	private FileOperations fileOperations;
	private BlockManager bsm;

	@Before
	public void setUp() throws Exception {
		this.fs = new FileSystem(100*1024*1024,1024);
		FileSystemOperations fileSystemOperations = new FileSystemOperations();
		this.fileOperations = new FileOperations(fs);
		
		fileSystemOperations.initFileSystem(fs);
		bsm = new BlockManager(this.fs.getRootBlockIndex(), this.fs.getBlocks());
		
		}

//	@Test
//	public void testGetBlockIndexesFromPathPathNotInBlock() {
//		ArrayList<Integer> result = bsm.getBlockIndexesFromPath("\\FolderA");
//		assertNull(result.get(0));
//		assertEquals(1, result.size());
//	}

}
