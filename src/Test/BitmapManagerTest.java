package Test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import block.FreeBlockBitmap;
import Controller.FileOperations;
import Controller.FileSystemOperations;
import Model.BitmapManager;
import Model.BlockManager;
import Run.FileSystem;

public class BitmapManagerTest {

	private FileOperations fileOperations;
	private BlockManager bsm;
	private FileSystem fs;

	@Before
	public void setUp() throws Exception {
		this.fs = new FileSystem(100*1024*1024,1024);
		FileSystemOperations fileSystemOperations = new FileSystemOperations();
		this.fileOperations = new FileOperations(fs);
		
		fileSystemOperations.initFileSystem(fs);
		bsm = new BlockManager(this.fs.getRootBlockIndex(), this.fs.getBlocks());
		
	}

//	@Test
//	public void test() {
//		System.out.println("Block Count: "+this.fs.getBlockCount());
//		System.out.println("FreeBlockBitmap Count: "+this.fs.getFreeBlockBitmapBlockCount());
//		System.out.println("FreeBlockBitmap size: " +((FreeBlockBitmap) this.fs.getBlocks()[1]).getFreeBitSize());
//		BitmapManager bm = new BitmapManager(this.fs.getBlocks(),1,this.fs.getFreeBlockBitmapBlockCount()+1);
//		int value1[] = bm.getFreeBlockBitmapAndBitIndex(1);
//		assertEquals(1,value1[0]);
//		assertEquals(1,value1[1]);
//		int value2[] = bm.getFreeBlockBitmapAndBitIndex(16192);
//		//System.out.println(value2[0]+","+value2[1]);
//		assertEquals(2,value2[0]);
//		assertEquals(0,value2[1]);
//
//	}

}
