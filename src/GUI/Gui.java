package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JSplitPane;

import java.awt.BorderLayout;

import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;

import java.awt.FlowLayout;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import Controller.Console;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class Gui {

	private JFrame frame;
	private JTextArea textArea_3;
	private Console console;
	private JTextArea textArea_1;
	private JTextArea textArea;
	private JTextArea textArea_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {	
					Gui window = new Gui();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	protected void setCode(String code) {
		this.textArea_3.setText(code);
	}

	/**
	 * Create the application.
	 */
	public Gui() {
		initialize();
		console = new Console();
		setCode(console.getInputString());
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("สมาชิกในกลุ่ม: 1.5510450398 นาย พศิน สุขใจมิตร 2.5510406666 แม็ก ยาขอบเซิน 3.5510405660 นนทวิชช์ ดวงสอดศรี");
		JSplitPane splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.1);
		frame.getContentPane().add(splitPane, BorderLayout.CENTER);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		splitPane.setRightComponent(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("1KB", null, panel, null);
		panel.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane);
		
		textArea_1 = new JTextArea();
		textArea_1.setFont(new Font("Monospaced", Font.PLAIN, 13));
		textArea_1.setTabSize(6);
		scrollPane.setViewportView(textArea_1);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("2KB", null, panel_1, null);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane_1 = new JScrollPane();
		panel_1.add(scrollPane_1);
		
		textArea = new JTextArea();
		textArea.setFont(new Font("Monospaced", Font.PLAIN, 13));
		textArea.setTabSize(6);
		scrollPane_1.setViewportView(textArea);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("4KB", null, panel_2, null);
		panel_2.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane_2 = new JScrollPane();
		panel_2.add(scrollPane_2);
		
		textArea_2 = new JTextArea();
		textArea_2.setFont(new Font("Monospaced", Font.PLAIN, 13));
		textArea_2.setTabSize(6);
		scrollPane_2.setViewportView(textArea_2);
		
		JPanel panel_3 = new JPanel();
		splitPane.setLeftComponent(panel_3);
		panel_3.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane_3 = new JScrollPane();
		panel_3.add(scrollPane_3);
		
		textArea_3 = new JTextArea();
		textArea_3.setFont(new Font("Monospaced", Font.PLAIN, 13));
		textArea_3.setTabSize(6);
		scrollPane_3.setViewportView(textArea_3);
		
		JMenuBar menuBar = new JMenuBar();
		frame.getContentPane().add(menuBar, BorderLayout.NORTH);
		
		JMenu mnMenu = new JMenu("Menu");
		menuBar.add(mnMenu);
		
		JMenuItem mntmExecute = new JMenuItem("Execute");
		mntmExecute.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textArea_1.setText(console.run1k(textArea_3.getText()));
				textArea.setText(console.run2k(textArea_3.getText()));
				textArea_2.setText(console.run4k(textArea_3.getText()));
			}
		});
		mnMenu.add(mntmExecute);
	}

}
