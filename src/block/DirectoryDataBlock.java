package block;

import java.util.ArrayList;
import java.util.HashMap;

public class DirectoryDataBlock extends Block {

	private ArrayList<String> dirNames = new ArrayList<String>();
	private ArrayList<String> fileNames = new ArrayList<String>();
	private HashMap<String, Integer> dmbChildAddresses = new HashMap<String, Integer>();
	private HashMap<String, Integer> fmbChildAddresses = new HashMap<String, Integer>();

	public boolean hasFileName(String name) {
		if (fileNames.contains(name)) {
			return true;
		}
		return false;
	}

	public boolean hasDirName(String name) {
		if (dirNames.contains(name)) {
			return true;
		}
		return false;
	}

	public ArrayList<Integer> getFmbChildAddresses() {
		ArrayList<Integer> result = new ArrayList<Integer>();
		for (Integer address : fmbChildAddresses.values()) {
			if (!result.contains(address)) {
				result.add(address);
			}
		}
		return result;
	}

	public ArrayList<Integer> getDmbChildAddresses() {
		ArrayList<Integer> result = new ArrayList<Integer>();
		for (Integer address : dmbChildAddresses.values()) {
			if (!result.contains(address)) {
				result.add(address);
			}
		}
		return result;
	}

	public void addFmbChildAddress(String fileName, int index) {
		this.fmbChildAddresses.put(fileName, index);
	}

	public void addDmbChildAddress(String dirName, int index) {
		this.dmbChildAddresses.put(dirName, index);

	}

	public void addFileName(String name) {
		this.fileNames.add(name);
	}

	public void addDirName(String name) {
		this.dirNames.add(name);
	}

	public void deleteFile(String name) {
		this.fileNames.remove(name);
		this.fmbChildAddresses.remove(name);
	}

	public String toString() {
		return "ddb";
	}

	public int getNumberOfFiles() {
		return this.fileNames.size();
	}

	public ArrayList<String> getDirNames() {
		return dirNames;
	}

	public ArrayList<String> getFileNames() {
		return fileNames;
	}

	public int getFmbChildAddressByName(String name) {
		return this.fmbChildAddresses.get(name);
	}

	public int getDmbChildAddressByName(String name) {
		return this.dmbChildAddresses.get(name);
	}

}
