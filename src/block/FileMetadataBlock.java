package block;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class FileMetadataBlock extends Block{

	private final SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private String fileName;
	private Date createdDate;
	private Date modifiedDate;
	private int linkCount;
	private int fileSize; //byte
	private ArrayList<Integer> fdbAddress = new ArrayList<Integer>();
//	private indirect block

	public FileMetadataBlock(String name){
		this.fileName = name;
	}
	public String getFileName(){
		return this.fileName;
	}
	public ArrayList<Integer> getFdbAddress(){
		return this.fdbAddress;
	}
	public void addFdbAddress(int index){
		this.fdbAddress.add(index);
	}
	public String toString(){
		return "fmb";
	}
	
	public int getFileSize() {
		return fileSize;
	}
	
	public void addFileSize(int fileSize) {
		this.fileSize += fileSize;
	}
	public void incrementLinkCount(){
		this.linkCount++;
	}
	public void decrementLinkCount(){
		this.linkCount--;
	}
	public int getLinkCount(){
		return this.linkCount;
	}
}
