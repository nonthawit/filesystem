package block;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DirectoryMetadataBlock extends Block{

	private final SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private String dirName;
	private Date createdDate;
	private Date modifiedDate;
	private int ddbAddress;

	public DirectoryMetadataBlock(String dirName){
		this.dirName = dirName;
	}
	public void setDdbAddress(int index){
		this.ddbAddress = index;
	}
	public Integer getDdbAddress() {
		return this.ddbAddress;
	}

	public Object getDirName() {
		return this.dirName;
	}
	public String toString(){
		return "dmb";
	}

}
