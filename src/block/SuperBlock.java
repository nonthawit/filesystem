package block;

import java.util.Date;
import java.text.SimpleDateFormat;

public class SuperBlock extends Block{
	private final SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private int diskSize;
	private int blockSize;
	private int rootDirectoryAddress;
	private Date checkDate;
	private Boolean state;
//	private int directAddressCount;
	
	public int getDiskSize() {
		return diskSize;
	}
	
	public int getBlockSize() {
		return blockSize;
	}
	
	public void setDiskSize(int diskSize) {
		this.diskSize = diskSize;
	}
	
	public void setBlockSize(int blockSize) {
		this.blockSize = blockSize;
	}
	
	public void setRootDirectoryAddress(int rootDirectoryAddress) {
		this.rootDirectoryAddress = rootDirectoryAddress;
	}
	public String toString(){
		return "sb";
	}
}
