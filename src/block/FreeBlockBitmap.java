package block;

import java.util.Arrays;

public class FreeBlockBitmap extends Block {

	private int freeBitSize;
	private boolean[] freeBitMap;

	public FreeBlockBitmap(int size) {
		this.freeBitSize = size;
		this.freeBitMap = new boolean[freeBitSize];
		this.setAllFreeBit();
	}

	public int getFreeBitSize() {
		return freeBitSize;
	}

	public boolean[] getFreeBitMap() {
		return freeBitMap;
	}

	public void setFreeBit(int index) {
		freeBitMap[index] = true;
	}

	public void setAllFreeBit() {
		Arrays.fill(this.freeBitMap, true);
	}

	public void setUseBit(int index) {
		freeBitMap[index] = false;
	}

	public void setAllUseBit() {
		Arrays.fill(this.freeBitMap, false);
	}

	@Override
	public String toString() {
		return "fbb";
	}
}
