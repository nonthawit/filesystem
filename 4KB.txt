สมาชิกในกลุ่ม
1.5510450398 นาย พศิน สุขใจมิตร
2.5510406666 แม็ก ยาขอบเซิน
3.5510405660 นนทวิชช์ ดวงสอดศรี

<<block size =4096>>
command entered:	createfile("one.txt")
			readdisk(2) 	-> directory data block
			writedisk(2) 	-> directory data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
Total operation time (r/w) : 4

--< Efficiency Report
Total number of block(s) : 4
Total number of file(s) : 1
Total file sizes : 0
Disk space efficiency : 0.0 %
--< End of report

<<block size =4096>>
command entered:	writefile("one.txt", "12288")
			readdisk(2) 	-> directory data block
			readdisk(3) 	-> file metadata block
			writedisk(4) 	-> file data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
			writedisk(5) 	-> file data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
			writedisk(6) 	-> file data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
Total operation time (r/w) : 11

--< Efficiency Report
Total number of block(s) : 7
Total number of file(s) : 1
Total file sizes : 12288
Disk space efficiency : 0.4286 %
--< End of report

<<block size =4096>>
command entered:	readfile("one.txt", "4096")
			readdisk(2) 	-> directory data block
			readdisk(3) 	-> file metadata block
Total operation time (r/w) : 2

<<block size =4096>>
command entered:	writefile("ont.txt", "10000")
Total operation time (r/w) : 0

--< Efficiency Report
Total number of block(s) : 7
Total number of file(s) : 1
Total file sizes : 12288
Disk space efficiency : 0.4286 %
--< End of report

<<block size =4096>>
command entered:	closefile("one.txt")
Total operation time (r/w) : 0

--< Consistency Report >--
<<block size =4096>>
Directory :[Name: "one.txt", Count: 0, Start: 3, Size: 12288]
File name		: "one.txt"
Block occupied: 	(4,5,6)
--< End of report

<<block size =4096>>
command entered:	createfile("two.txt")
			readdisk(2) 	-> directory data block
			writedisk(2) 	-> directory data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
Total operation time (r/w) : 4

--< Efficiency Report
Total number of block(s) : 8
Total number of file(s) : 2
Total file sizes : 12288
Disk space efficiency : 0.375 %
--< End of report

<<block size =4096>>
command entered:	writefile("two.txt", "1024")
			readdisk(2) 	-> directory data block
			readdisk(7) 	-> file metadata block
			writedisk(8) 	-> file data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
Total operation time (r/w) : 5

--< Efficiency Report
Total number of block(s) : 9
Total number of file(s) : 2
Total file sizes : 13312
Disk space efficiency : 0.3611 %
--< End of report

<<block size =4096>>
command entered:	writefile("two.txt", "500")
			readdisk(2) 	-> directory data block
			readdisk(7) 	-> file metadata block
			writedisk(9) 	-> file data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
Total operation time (r/w) : 5

--< Efficiency Report
Total number of block(s) : 10
Total number of file(s) : 2
Total file sizes : 13812
Disk space efficiency : 0.3372 %
--< End of report

<<block size =4096>>
command entered:	closefile("two.txt")
Total operation time (r/w) : 0

<<block size =4096>>
command entered:	linkfile("two.txt", "\new.txt")
			readdisk(2) 	-> directory data block
			readdisk(2) 	-> directory data block
			writedisk(2) 	-> directory data block
			readdisk(7) 	-> file metadata block
			writedisk(7) 	-> file metadata block
Total operation time (r/w) : 5

--< Efficiency Report
Total number of block(s) : 10
Total number of file(s) : 3
Total file sizes : 13812
Disk space efficiency : 0.3372 %
--< End of report

--< Consistency Report >--
<<block size =4096>>
Directory :[Name: "one.txt", Count: 0, Start: 3, Size: 12288]
[Name: "two.txt", Count: 1, Start: 7, Size: 1524]
[Name: "new.txt", Count: 1, Start: 7, Size: 1524]
File name		: "one.txt"
Block occupied: 	(4,5,6)
File name		: "two.txt"
Block occupied: 	(8,9)
File name		: "new.txt"
Block occupied: 	(8,9)
--< End of report

<<block size =4096>>
command entered:	createfile("paper.txt")
			readdisk(2) 	-> directory data block
			writedisk(2) 	-> directory data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
Total operation time (r/w) : 4

--< Efficiency Report
Total number of block(s) : 11
Total number of file(s) : 4
Total file sizes : 13812
Disk space efficiency : 0.3066 %
--< End of report

<<block size =4096>>
command entered:	writefile("paper.txt", "12800")
			readdisk(2) 	-> directory data block
			readdisk(10) 	-> file metadata block
			writedisk(11) 	-> file data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
			writedisk(12) 	-> file data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
			writedisk(13) 	-> file data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
			writedisk(14) 	-> file data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
Total operation time (r/w) : 14

--< Efficiency Report
Total number of block(s) : 15
Total number of file(s) : 4
Total file sizes : 26612
Disk space efficiency : 0.4331 %
--< End of report

<<block size =4096>>
command entered:	readfile("paper.txt", "7000")
			readdisk(2) 	-> directory data block
			readdisk(10) 	-> file metadata block
Total operation time (r/w) : 2

<<block size =4096>>
command entered:	closefile("paper.txt")
Total operation time (r/w) : 0

--< Consistency Report >--
<<block size =4096>>
Directory :[Name: "one.txt", Count: 0, Start: 3, Size: 12288]
[Name: "two.txt", Count: 1, Start: 7, Size: 1524]
[Name: "new.txt", Count: 1, Start: 7, Size: 1524]
[Name: "paper.txt", Count: 0, Start: 10, Size: 12800]
File name		: "one.txt"
Block occupied: 	(4,5,6)
File name		: "two.txt"
Block occupied: 	(8,9)
File name		: "new.txt"
Block occupied: 	(8,9)
File name		: "paper.txt"
Block occupied: 	(11,12,13,14)
--< End of report

<<block size =4096>>
command entered:	createfile("report.txt")
			readdisk(2) 	-> directory data block
			writedisk(2) 	-> directory data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
Total operation time (r/w) : 4

--< Efficiency Report
Total number of block(s) : 16
Total number of file(s) : 5
Total file sizes : 26612
Disk space efficiency : 0.4061 %
--< End of report

<<block size =4096>>
command entered:	writefile("report.txt", "4352")
			readdisk(2) 	-> directory data block
			readdisk(15) 	-> file metadata block
			writedisk(16) 	-> file data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
			writedisk(17) 	-> file data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
Total operation time (r/w) : 8

--< Efficiency Report
Total number of block(s) : 18
Total number of file(s) : 5
Total file sizes : 30964
Disk space efficiency : 0.42 %
--< End of report

<<block size =4096>>
command entered:	readfile("report.txt", "1000")
			readdisk(2) 	-> directory data block
			readdisk(15) 	-> file metadata block
Total operation time (r/w) : 2

<<block size =4096>>
command entered:	readfile("report.txt", "200")
			readdisk(2) 	-> directory data block
			readdisk(15) 	-> file metadata block
Total operation time (r/w) : 2

<<block size =4096>>
command entered:	closefile("report.txt")
Total operation time (r/w) : 0

--< Consistency Report >--
<<block size =4096>>
Directory :[Name: "one.txt", Count: 0, Start: 3, Size: 12288]
[Name: "two.txt", Count: 1, Start: 7, Size: 1524]
[Name: "new.txt", Count: 1, Start: 7, Size: 1524]
[Name: "paper.txt", Count: 0, Start: 10, Size: 12800]
[Name: "report.txt", Count: 0, Start: 15, Size: 4352]
File name		: "one.txt"
Block occupied: 	(4,5,6)
File name		: "two.txt"
Block occupied: 	(8,9)
File name		: "new.txt"
Block occupied: 	(8,9)
File name		: "paper.txt"
Block occupied: 	(11,12,13,14)
File name		: "report.txt"
Block occupied: 	(16,17)
--< End of report

<<block size =4096>>
command entered:	createfile("Feb.txt")
			readdisk(2) 	-> directory data block
			writedisk(2) 	-> directory data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
Total operation time (r/w) : 4

--< Efficiency Report
Total number of block(s) : 19
Total number of file(s) : 6
Total file sizes : 30964
Disk space efficiency : 0.3979 %
--< End of report

<<block size =4096>>
command entered:	createfile("Jun.txt")
			readdisk(2) 	-> directory data block
			writedisk(2) 	-> directory data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
Total operation time (r/w) : 4

--< Efficiency Report
Total number of block(s) : 20
Total number of file(s) : 7
Total file sizes : 30964
Disk space efficiency : 0.378 %
--< End of report

<<block size =4096>>
command entered:	writefile("Jun.txt", "11776")
			readdisk(2) 	-> directory data block
			readdisk(19) 	-> file metadata block
			writedisk(20) 	-> file data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
			writedisk(21) 	-> file data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
			writedisk(22) 	-> file data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
Total operation time (r/w) : 11

--< Efficiency Report
Total number of block(s) : 23
Total number of file(s) : 7
Total file sizes : 42740
Disk space efficiency : 0.4537 %
--< End of report

<<block size =4096>>
command entered:	writefile("Feb.txt", "9728")
			readdisk(2) 	-> directory data block
			readdisk(18) 	-> file metadata block
			writedisk(23) 	-> file data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
			writedisk(24) 	-> file data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
			writedisk(25) 	-> file data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
Total operation time (r/w) : 11

--< Efficiency Report
Total number of block(s) : 26
Total number of file(s) : 7
Total file sizes : 52468
Disk space efficiency : 0.4927 %
--< End of report

<<block size =4096>>
command entered:	closefile("Feb.txt")
Total operation time (r/w) : 0

<<block size =4096>>
command entered:	closefile("Jun.txt")
Total operation time (r/w) : 0

--< Consistency Report >--
<<block size =4096>>
Directory :[Name: "one.txt", Count: 0, Start: 3, Size: 12288]
[Name: "two.txt", Count: 1, Start: 7, Size: 1524]
[Name: "new.txt", Count: 1, Start: 7, Size: 1524]
[Name: "paper.txt", Count: 0, Start: 10, Size: 12800]
[Name: "report.txt", Count: 0, Start: 15, Size: 4352]
[Name: "Feb.txt", Count: 0, Start: 18, Size: 9728]
[Name: "Jun.txt", Count: 0, Start: 19, Size: 11776]
File name		: "one.txt"
Block occupied: 	(4,5,6)
File name		: "two.txt"
Block occupied: 	(8,9)
File name		: "new.txt"
Block occupied: 	(8,9)
File name		: "paper.txt"
Block occupied: 	(11,12,13,14)
File name		: "report.txt"
Block occupied: 	(16,17)
File name		: "Feb.txt"
Block occupied: 	(23,24,25)
File name		: "Jun.txt"
Block occupied: 	(20,21,22)
--< End of report

<<block size =4096>>
command entered:	linkfile("Feb.txt", "\Oct.txt")
			readdisk(2) 	-> directory data block
			readdisk(2) 	-> directory data block
			writedisk(2) 	-> directory data block
			readdisk(18) 	-> file metadata block
			writedisk(18) 	-> file metadata block
Total operation time (r/w) : 5

--< Efficiency Report
Total number of block(s) : 26
Total number of file(s) : 8
Total file sizes : 52468
Disk space efficiency : 0.4927 %
--< End of report

<<block size =4096>>
command entered:	linkfile("Oct.txt", "\Nov.txt")
			readdisk(2) 	-> directory data block
			readdisk(2) 	-> directory data block
			writedisk(2) 	-> directory data block
			readdisk(18) 	-> file metadata block
			writedisk(18) 	-> file metadata block
Total operation time (r/w) : 5

--< Efficiency Report
Total number of block(s) : 26
Total number of file(s) : 9
Total file sizes : 52468
Disk space efficiency : 0.4927 %
--< End of report

<<block size =4096>>
command entered:	linkfile("paper.txt", "\newpaper.txt")
			readdisk(2) 	-> directory data block
			readdisk(2) 	-> directory data block
			writedisk(2) 	-> directory data block
			readdisk(10) 	-> file metadata block
			writedisk(10) 	-> file metadata block
Total operation time (r/w) : 5

--< Efficiency Report
Total number of block(s) : 26
Total number of file(s) : 10
Total file sizes : 52468
Disk space efficiency : 0.4927 %
--< End of report

<<block size =4096>>
command entered:	deletefile("Oct.txt")
			readdisk(2) 	-> directory data block
			writedisk(2) 	-> directory data block
			readdisk(18) 	-> file metadata block
			writedisk(18) 	-> file metadata block
Total operation time (r/w) : 4

--< Efficiency Report
Total number of block(s) : 26
Total number of file(s) : 9
Total file sizes : 52468
Disk space efficiency : 0.4927 %
--< End of report

<<block size =4096>>
command entered:	deletefile("paper.txt")
			readdisk(2) 	-> directory data block
			writedisk(2) 	-> directory data block
			readdisk(10) 	-> file metadata block
			writedisk(10) 	-> file metadata block
Total operation time (r/w) : 4

--< Efficiency Report
Total number of block(s) : 26
Total number of file(s) : 8
Total file sizes : 52468
Disk space efficiency : 0.4927 %
--< End of report

<<block size =4096>>
command entered:	deletefile("report.txt")
			readdisk(2) 	-> directory data block
			writedisk(2) 	-> directory data block
			readdisk(15) 	-> file metadata block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
Total operation time (r/w) : 9

--< Efficiency Report
Total number of block(s) : 23
Total number of file(s) : 7
Total file sizes : 48116
Disk space efficiency : 0.5107 %
--< End of report

<<block size =4096>>
command entered:	openfile("newPaper.txt")
Total operation time (r/w) : 0

<<block size =4096>>
command entered:	writefile("newPaper.txt", "2048")
Total operation time (r/w) : 0

--< Efficiency Report
Total number of block(s) : 23
Total number of file(s) : 7
Total file sizes : 48116
Disk space efficiency : 0.5107 %
--< End of report

<<block size =4096>>
command entered:	openfile("two.txt")
Total operation time (r/w) : 0

<<block size =4096>>
command entered:	getoffset("two.txt")
offset : 	7

Total operation time (r/w) : 0

<<block size =4096>>
command entered:	readfile("two.txt", "100")
			readdisk(2) 	-> directory data block
			readdisk(7) 	-> file metadata block
Total operation time (r/w) : 2

<<block size =4096>>
command entered:	getoffset("two.txt")
offset : 	7

Total operation time (r/w) : 0

<<block size =4096>>
command entered:	closefile("wo.txt")
Total operation time (r/w) : 0

<<block size =4096>>
command entered:	closefile("ewPaper.txt")
Total operation time (r/w) : 0

--< Consistency Report >--
<<block size =4096>>
Directory :[Name: "one.txt", Count: 0, Start: 3, Size: 12288]
[Name: "two.txt", Count: 1, Start: 7, Size: 1524]
[Name: "new.txt", Count: 1, Start: 7, Size: 1524]
[Name: "Feb.txt", Count: 1, Start: 18, Size: 9728]
[Name: "Jun.txt", Count: 0, Start: 19, Size: 11776]
[Name: "Nov.txt", Count: 1, Start: 18, Size: 9728]
[Name: "newpaper.txt", Count: 0, Start: 10, Size: 12800]
File name		: "one.txt"
Block occupied: 	(4,5,6)
File name		: "two.txt"
Block occupied: 	(8,9)
File name		: "new.txt"
Block occupied: 	(8,9)
File name		: "Feb.txt"
Block occupied: 	(23,24,25)
File name		: "Jun.txt"
Block occupied: 	(20,21,22)
File name		: "Nov.txt"
Block occupied: 	(23,24,25)
File name		: "newpaper.txt"
Block occupied: 	(11,12,13,14)
--< End of report

<<block size =4096>>
command entered:	createdirectory("FolderA")
			readdisk(2) 	-> directory data block
			writedisk(2) 	-> directory data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
			writedisk(15) 	-> directory metadata block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
			writedisk(16) 	-> directory data block
Total operation time (r/w) : 8

--< Efficiency Report
Total number of block(s) : 25
Total number of file(s) : 7
Total file sizes : 48116
Disk space efficiency : 0.4699 %
--< End of report

<<block size =4096>>
command entered:	createdirectory("FolderA\FolderA-1")
			readdisk(15) 	-> directory metadata block
			readdisk(16) 	-> directory data block
			writedisk(16) 	-> directory data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
			writedisk(17) 	-> directory metadata block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
			writedisk(26) 	-> directory data block
Total operation time (r/w) : 9

--< Efficiency Report
Total number of block(s) : 27
Total number of file(s) : 7
Total file sizes : 48116
Disk space efficiency : 0.4351 %
--< End of report

<<block size =4096>>
command entered:	createfile("FolderA\fileA.dat")
			readdisk(15) 	-> directory metadata block
			readdisk(16) 	-> directory data block
			writedisk(16) 	-> directory data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
Total operation time (r/w) : 5

--< Efficiency Report
Total number of block(s) : 28
Total number of file(s) : 8
Total file sizes : 48116
Disk space efficiency : 0.4195 %
--< End of report

<<block size =4096>>
command entered:	linkfile("newpaper.txt", "\FolderA\FolderA-1\mypaper.txt")
			readdisk(2) 	-> directory data block
			readdisk(17) 	-> directory metadata block
			readdisk(26) 	-> directory data block
			writedisk(26) 	-> directory data block
			readdisk(10) 	-> file metadata block
			writedisk(10) 	-> file metadata block
Total operation time (r/w) : 6

--< Efficiency Report
Total number of block(s) : 28
Total number of file(s) : 9
Total file sizes : 48116
Disk space efficiency : 0.4195 %
--< End of report

<<block size =4096>>
command entered:	writefile("FolderA\fileA.dat", "26820")
			readdisk(15) 	-> directory metadata block
			readdisk(16) 	-> directory data block
			readdisk(27) 	-> file metadata block
			writedisk(28) 	-> file data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
			writedisk(29) 	-> file data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
			writedisk(30) 	-> file data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
			writedisk(31) 	-> file data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
			writedisk(32) 	-> file data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
			writedisk(33) 	-> file data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
			writedisk(34) 	-> file data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
Total operation time (r/w) : 24

--< Efficiency Report
Total number of block(s) : 35
Total number of file(s) : 9
Total file sizes : 74936
Disk space efficiency : 0.5227 %
--< End of report

<<block size =4096>>
command entered:	writefile("FolderA\fileA.dat", "16520")
			readdisk(15) 	-> directory metadata block
			readdisk(16) 	-> directory data block
			readdisk(27) 	-> file metadata block
			writedisk(35) 	-> file data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
			writedisk(36) 	-> file data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
			writedisk(37) 	-> file data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
			writedisk(38) 	-> file data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
			writedisk(39) 	-> file data block
			readdisk(1) 	-> free block bitmap
			writedisk(1) 	-> free block bitmap
Total operation time (r/w) : 18

--< Efficiency Report
Total number of block(s) : 40
Total number of file(s) : 9
Total file sizes : 91456
Disk space efficiency : 0.5582 %
--< End of report

<<block size =4096>>
command entered:	seekfile("FolderA\fileA.dat", "10400")
previous postion : 	0
current position : 	10400

Total operation time (r/w) : 0

<<block size =4096>>
command entered:	readfile("FolderA\fileA.dat", "2350")
			readdisk(15) 	-> directory metadata block
			readdisk(16) 	-> directory data block
			readdisk(27) 	-> file metadata block
Total operation time (r/w) : 3

<<block size =4096>>
command entered:	getsize("FolderA\fileA.dat")
fileSize : 	43340

			readdisk(27) 	-> file metadata block
Total operation time (r/w) : 1

<<block size =4096>>
command entered:	listopenfile()
Open File List : 	[fileA.dat]

Total operation time (r/w) : 0

<<block size =4096>>
command entered:	closefile("FolderA\fileA.dat")
Total operation time (r/w) : 0

--< Consistency Report >--
<<block size =4096>>
Directory :[Name: "one.txt", Count: 0, Start: 3, Size: 12288]
[Name: "two.txt", Count: 1, Start: 7, Size: 1524]
[Name: "new.txt", Count: 1, Start: 7, Size: 1524]
[Name: "Feb.txt", Count: 1, Start: 18, Size: 9728]
[Name: "Jun.txt", Count: 0, Start: 19, Size: 11776]
[Name: "Nov.txt", Count: 1, Start: 18, Size: 9728]
[Name: "newpaper.txt", Count: 1, Start: 10, Size: 12800]
[Name: "fileA.dat", Count: 0, Start: 27, Size: 43340]
[Name: "mypaper.txt", Count: 1, Start: 10, Size: 12800]
File name		: "one.txt"
Block occupied: 	(4,5,6)
File name		: "two.txt"
Block occupied: 	(8,9)
File name		: "new.txt"
Block occupied: 	(8,9)
File name		: "Feb.txt"
Block occupied: 	(23,24,25)
File name		: "Jun.txt"
Block occupied: 	(20,21,22)
File name		: "Nov.txt"
Block occupied: 	(23,24,25)
File name		: "newpaper.txt"
Block occupied: 	(11,12,13,14)
File name		: "fileA.dat"
Block occupied: 	(28,29,30,31,32,33,34,35,36,37,38,39)
File name		: "mypaper.txt"
Block occupied: 	(11,12,13,14)
--< End of report

